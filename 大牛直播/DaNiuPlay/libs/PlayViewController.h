//
//  PlayViewController.h
//  EmergencySystem
//
//  Created by Keith on 2018/1/2.
//  Copyright © 2018年 Keith. All rights reserved.
//

#import "BaseVC.h"
#import "SmartPlayerSDK.h"
@interface PlayViewController : BaseVC<SmartPlayerDelegate>
- (instancetype)initParameter:(NSString*)url higth:(NSInteger)higth
                   bufferTime:(NSInteger)bufferTime
                isFastStartup:(Boolean)isFastStartup
                 isLowLantecy:(Boolean)isLowLantecy
                  isHWDecoder:(Boolean)isHWDecoder
                isRTSPTcpMode:(Boolean)isRTSPTcpMode;

@property (nonatomic,strong)SmartPlayerSDK  * player;
@property (nonatomic,strong)UIView          * glView;

@property (nonatomic,assign)rotaType rotype;


@end
