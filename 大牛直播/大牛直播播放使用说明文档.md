
# 大牛直播播放使用文档


## 前沿

* 项目中的视频直播播放功能用的大牛sdk。






## 主要内容的介绍

* `推流`<br>

* `播放视频`<br>


## 直播 集成


* 拖拽 “DaNiuPlay”"DaNiuPush" 文件夹到工程中


## 代码介绍 (详细使用，请参考 Demo)

#### 1、在 info.plist 中添加以下字段

* Privacy - Camera Usage Description (相机权限访问)`<br>



#### 2、直播使用

* 推流

```Objective-C

//audioOpt 0不推音频，1推音频
//videoOpt 0不推视频，1推视频
//isBeauty 是否美颜
DN_VIDEO_QUALITY_LOW,           //!< 视频分辨率：低清.
DN_VIDEO_QUALITY_MEDIUM,        //!< 视频分辨率：标清.
DN_VIDEO_QUALITY_HIGH           //!< 视频分辨率：高清.

   ViewController * coreView =[[ViewController alloc] initParameter:@"rtmpurl" streamQuality:DN_VIDEO_QUALITY_MEDIUM audioOpt:0 videoOpt:1 isBeauty:NO];
   
```

* 拉流

```Objective-C

    Boolean         is_fast_startup_;           //是否快速启动模式
    Boolean         is_low_latency_mode_;       //是否开启极速模式
    NSInteger       buffer_time_;               //buffer时间
    Boolean         is_hardware_decoder_;       //默认软解码
    Boolean         is_rtsp_tcp_mode_;          //仅用于rtsp流，设置TCP传输模式

 PlayViewController * vc =[[PlayViewController alloc] initParameter:self.path
                                                                 higth:高度
                                                                  bufferTime:0
                                                               isFastStartup:YES
                                                                isLowLantecy:YES
                                                                 isHWDecoder:NO
                                                               isRTSPTcpMode:NO]; 
```

* 推流销毁


```Objective-C

  [self.coreView.smart_publisher_sdk SmartPublisherStopPublisher];
    [self.coreView.smart_publisher_sdk SmartPublisherUnInit];
    self.coreView.smart_publisher_sdk = nil;
    [self.coreView  removeFromParentViewController];
    [self.coreView.view removeFromSuperview];
    self.coreView=nil;
 
```

* 拉流销毁

```Objective-C

    [self.vc.player SmartPlayerStop];
    [self.vc.player SmartPlayerUnInitPlayer];
 
    self.vc.player = nil;
    [self.vc.glView removeFromSuperview];
    [SmartPlayerSDK SmartPlayeReleasePlayView:(__bridge void *)(self.vc.glView)];
    self.vc.glView = nil;
   
    [self.vc.view removeFromSuperview];
    [self.vc removeFromParentViewController];
    self.vc=nil;
```


## 更新介绍




