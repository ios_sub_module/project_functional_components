//
//  ShowMapVC.m
//  EmergencySystem
//
//  Created by Keith on 2017/6/29.
//  Copyright © 2017年 Keith. All rights reserved.
//

#import "ShowMapVC.h"
#import <MapKit/MapKit.h>
#import "XMGAnno.h"
@interface ShowMapVC ()<MKMapViewDelegate>
@property(nonatomic,strong)MKMapView *mapView;


@property (nonatomic, strong) CLGeocoder *geoC;
@end

@implementation ShowMapVC
- (CLGeocoder *)geoC
{
    if (!_geoC) {
        _geoC = [[CLGeocoder alloc] init];
    }
    return _geoC;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNav];
    [self setContent];
    
    if (self.lat) {
        // 设置地图显示区域
        MKCoordinateSpan span = MKCoordinateSpanMake(0.011256, 0.006093);
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([self.lat doubleValue],[self.lng doubleValue]);
        MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, span);
        [self.mapView setRegion:region animated:NO];
        __block XMGAnno *anno = [[XMGAnno alloc] init];
        anno.coordinate = coordinate;
        anno.title = @"厦门市";
        [self.mapView addAnnotation:anno];
        [self.mapView selectAnnotation:anno animated:YES];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:anno.coordinate.latitude longitude:anno.coordinate.longitude];
        [self.geoC reverseGeocodeLocation:loc completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            CLPlacemark *pl = [placemarks firstObject];
            anno.title =[NSString stringWithFormat:@"(%@)%@",self.name,pl.locality] ;
            anno.subtitle = [NSString stringWithFormat:@"%@",pl.thoroughfare?:pl.name?:pl.subLocality];
            
            
        }];
    }else
    {
         [self getmapAdress];
    }
   
    ////用户拒绝该应用使用定位服务,或者定位服务处于关闭状态
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        
        [Tool showAlertControllerWithTitleNOCancel:@"打开定位开关" andMessageNOCancel:@"定位服务未开启，请进入系统【设置】>【隐私】>【定位服务】中打开开关，并允许通信宝使用定位服务" andConfirmHandleNOCancel:^(NSString *str) {
            
        }];
        
    }
    
}


-(void)getmapAdress
{
    WGetWeakSelf(weak, self);
    [self.netManager getmapAdressWithTokan:USERMODEL.token
                                  andtelno:self.telno
                                    Result:^(id serverMdl) {
                                        
                                        if ([serverMdl[@"errcode"]integerValue]==0) {
                                            
                                            weak.lat=serverMdl[@"data"][@"latitude"];
                                            weak.lng=serverMdl[@"data"][@"longitude"];
                                            weak.lastTime=serverMdl[@"data"][@"datetime"];
                                            
                                            if (weak.lat&&weak.lng&&![weak.lat isEqualToString:@"0"]&&![weak.lng isEqualToString:@"0"]) {
                                                // 设置地图显示区域
                                                MKCoordinateSpan span = MKCoordinateSpanMake(0.011256, 0.006093);
                                                CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([weak.lat doubleValue],[weak.lng doubleValue]);
                                                MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, span);
                                                [weak.mapView setRegion:region animated:NO];
                                                __block XMGAnno *anno = [[XMGAnno alloc] init];
                                                anno.coordinate = coordinate;
                                                anno.title = @"厦门市";
                                                [weak.mapView addAnnotation:anno];
                                                [weak.mapView selectAnnotation:anno animated:YES];
                                                CLLocation *loc = [[CLLocation alloc] initWithLatitude:anno.coordinate.latitude longitude:anno.coordinate.longitude];
                                                [self.geoC reverseGeocodeLocation:loc completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
                                                    CLPlacemark *pl = [placemarks firstObject];
                                                    anno.title =[NSString stringWithFormat:@"(%@)%@",weak.name,pl.locality] ;
                                                    anno.subtitle = [NSString stringWithFormat:@"%@,%@",pl.thoroughfare?:pl.name?:pl.subLocality,weak.lastTime];
                                                    }];
                                            }else
                                            {
                                                [Tool showAlertControllerWithTitleNOCancel:@"用户未开启定位，无法获取位置" andMessageNOCancel:nil andConfirmHandleNOCancel:^(NSString *str) {
                                                    [weak.navigationController popViewControllerAnimated:YES];
                                                }];
                                                
                                            }

                                        }

                                        
                                    } Error:^(NSError *err) {
                                        
                                    } RefreshToken:^{
                                        
                                    }];
}
-(MKMapView *)mapView
{
    if (_mapView == nil) {
        _mapView = [MKMapView new];
        _mapView.showsUserLocation = NO;
        
        _mapView.delegate=self;
        
        
    }
    return _mapView;
}

-(void)createNav
{
    
    self.title=@"位置";
    self.view.backgroundColor=HFRGBColor(243, 243, 243);
    
    
}


-(void)setContent
{
    [self.view addSubview:self.mapView];
    [self.mapView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self.view);
        
    }];
    
    
    
}


//- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
//    
//    if (annotation == mapView.userLocation) return nil;
//    
//    static NSString *inden = @"datouzhen";
//    MKPinAnnotationView *pin = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:inden];
//    
//    if (pin == nil) {
//        pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:inden];
//    }
//    
//    pin.annotation = annotation;
//    
//    // 设置是否弹出标注
//    pin.canShowCallout = YES;
//    
//    // 设置大头针颜色
//    pin.pinTintColor = [UIColor blackColor];
//    
//    // 从天而降
//    pin.animatesDrop = YES;
//    
//    // 设置大头针图片(系统大头针无效)
//    pin.image = [UIImage imageNamed:@"不说话"];
//    
//    pin.draggable = YES;
//    return pin;
//    
//    
//}





@end
