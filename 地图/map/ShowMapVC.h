//
//  ShowMapVC.h
//  EmergencySystem
//
//  Created by Keith on 2017/6/29.
//  Copyright © 2017年 Keith. All rights reserved.
//

#import "BaseVC.h"

@interface ShowMapVC : BaseVC
@property (nonatomic,copy)NSString *lat;
@property (nonatomic,copy)NSString *lng;
@property (nonatomic,copy)NSString *lastTime;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *telno;
@end
