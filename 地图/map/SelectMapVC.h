//
//  SelectMapVC.h
//  EmergencySystem
//
//  Created by Keith on 2017/6/28.
//  Copyright © 2017年 Keith. All rights reserved.
//

#import "BaseVC.h"

@interface SelectMapVC : BaseVC
@property (nonatomic,copy)NSString *lat;
@property (nonatomic,copy)NSString *lng;
@property (copy, nonatomic) void (^(selectMapBlock)) (NSString *name,NSString *lat,NSString *lng);

@end
