//
//  SelectMapVC.m
//  EmergencySystem
//
//  Created by Keith on 2017/6/28.
//  Copyright © 2017年 Keith. All rights reserved.
//

#import "SelectMapVC.h"

#import <MapKit/MapKit.h>
@interface SelectMapVC ()<MKMapViewDelegate>
@property(nonatomic,strong)MKMapView *mapView;
@property (nonatomic,strong)UIImageView *pinImageV;
@property (nonatomic,copy)NSString *addressTV;

@property (nonatomic, strong) CLGeocoder *geoC;

@property (nonatomic,assign)BOOL isfirst;//是否首次调用

@end

@implementation SelectMapVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [self createNav];
    [self setContent];


    self.isfirst=YES;
    // 设置地图显示区域
    MKCoordinateSpan span = MKCoordinateSpanMake(0.021256, 0.016093);
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([self.lat doubleValue],[self.lng doubleValue]);
    MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, span);
    [self.mapView setRegion:region animated:NO];
    
        
       
        
   

    
    


}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    ////用户拒绝该应用使用定位服务,或者定位服务处于关闭状态
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        
        [Tool showAlertControllerWithTitleNOCancel:@"打开定位开关" andMessageNOCancel:@"定位服务未开启，请进入系统【设置】>【隐私】>【定位服务】中打开开关，并允许应急通信中心使用定位服务" andConfirmHandleNOCancel:^(NSString *str) {
            
        }];
        
    }
    
}

-(CLGeocoder *)geoC
{
    if (!_geoC) {
        _geoC = [[CLGeocoder alloc] init];
    }
    return _geoC;
}

-(MKMapView *)mapView
{
    if (_mapView == nil) {
        _mapView = [MKMapView new];
        _mapView.showsUserLocation = YES;
        
        _mapView.delegate=self;
      
        
    }
    return _mapView;
}
-(UIImageView *)pinImageV
{
    if (_pinImageV==nil) {
        _pinImageV=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"大头针"]];
    }
    return _pinImageV;
}

-(void)createNav
{
    
    self.title=@"位置";
    self.view.backgroundColor=HFRGBColor(243, 243, 243);
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(rightAction)];
    [rightBtn setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = rightBtn;
    
    
}


-(void)setContent
{
    [self.view addSubview:self.mapView];
    [self.mapView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self.view);
        
    }];
    
    [self.view addSubview:self.pinImageV];
    [self.pinImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(20, 40));
    }];
    
    
    
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    if (self.isfirst) {
        self.isfirst=NO;
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[self.lat doubleValue] longitude:[self.lng doubleValue]];
        
        // 反地理编码(经纬度---地址)
        [self.geoC reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            if(error == nil)
            {
                CLPlacemark *pl = [placemarks firstObject];
                self.addressTV = pl.name;
                
                self.lat = @(pl.location.coordinate.latitude).stringValue;
                self.lng = @(pl.location.coordinate.longitude).stringValue;
                
                
            }else
            {
                NSLog(@"错误");
            }
        }];
        
    }else
    {
        MKCoordinateRegion region;
        CLLocationCoordinate2D centerCoordinate = mapView.region.center;
        region.center= centerCoordinate;
        
        NSLog(@" regionDidChangeAnimated %f,%f",centerCoordinate.latitude, centerCoordinate.longitude);
        //    self.lat= [NSString stringWithFormat:@"%lf",centerCoordinate.latitude];
        //    self.lng= [NSString stringWithFormat:@"%lf",centerCoordinate.longitude];
        //
        CLLocation *location = [[CLLocation alloc] initWithLatitude:centerCoordinate.latitude longitude:centerCoordinate.longitude];
        
        // 反地理编码(经纬度---地址)
        [self.geoC reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            if(error == nil)
            {
                CLPlacemark *pl = [placemarks firstObject];
                self.addressTV = pl.name;
                
                self.lat = @(pl.location.coordinate.latitude).stringValue;
                self.lng = @(pl.location.coordinate.longitude).stringValue;
                
                
            }else
            {
                NSLog(@"错误");
            }
        }];
    }
    
   
    
}


-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    CLLocationCoordinate2D coord = [userLocation coordinate];
    NSLog(@"latitude: %f, longitude: %f",coord.latitude, coord.longitude);
    

    CLLocation *location = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];
    // 反地理编码(经纬度---地址)
    [self.geoC reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if(error == nil)
        {
            CLPlacemark *pl = [placemarks firstObject];
            if (self.isWorkDetail) {
                self.lat = @(pl.location.coordinate.latitude).stringValue;
                self.lng = @(pl.location.coordinate.longitude).stringValue;
              
                
                // 设置地图显示区域
                MKCoordinateSpan span = MKCoordinateSpanMake(0.021256, 0.016093);
                CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([self.lat doubleValue],[self.lng doubleValue]);
                MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, span);
                [self.mapView setRegion:region animated:NO];
                self.isWorkDetail=NO;
            }
            
            
        }else
        {
            NSLog(@"错误");
        }
    }];
    
    
}


-(void)rightAction
{
    
    if (!self.addressTV) {
        
        [WProgressHudTool wShowAutoDismissWithStatus:NO Title:@"位置为空，请重新选择"];
        
    }else
    {
        self.selectMapBlock(self.addressTV,self.lat,self.lng);
        
        [self.navigationController popViewControllerAnimated:YES];

    }
    

    
    
   
    
}
@end
