
# socket使用文档


## 前沿

* 如果需要在项目中像QQ微信一样做到即时通讯，必须使用socket通讯。

* iphone的CFNetwork编程比较复杂。使用AsyncSocket开源库来开发相对较简单，帮助我们封装了很多东西


## 主要内容的介绍

* `消息推送`<br>

* `及时通信`<br>



## AsyncSocket 集成


* 拖拽 “GCD” 文件夹到工程中


## 代码介绍 (详细使用，请参考 Demo)

#### 常用方法

1、建立连接

```Objective-C

- (int)connectServer:(NSString *)hostIP port:(int)hostPort

```

2、连接成功后，会回调的函数

```Objective-C
- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port

```
3、发送数据

```Objective-C
- (void)writeData:(NSData *)data withTimeout:(NSTimeInterval)timeout tag:(long)tag;

```
4、接受数据

```Objective-C
-(void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag

```
5、断开连接

```Objective-C
- (void)onSocket:(AsyncSocket *)sock willDisconnectWithError:(NSError *)err

```

```Objective-C
- (void)onSocketDidDisconnect:(AsyncSocket *)sock

```



## 更新介绍




