# 项目功能组件

[socket使用文档](https://gitee.com/ios_sub_module/project_functional_components/blob/master/socket/socket使用文档.md)

[大牛直播使用文档](https://gitee.com/ios_sub_module/project_functional_components/blob/master/大牛直播/大牛直播播放使用说明文档.md)

[pjsip使用文档](https://gitee.com/ios_sub_module/project_functional_components/blob/master/pjsip/pjsip使用文档.md)

[web交互使用文档](https://gitee.com/ios_sub_module/project_functional_components/blob/master/web交互/web交互使用文档.md)

[APP与Web交互协议](https://gitee.com/ios_sub_module/project_functional_components/blob/master/web交互/APP与Web交互协议.md)

[二维码使用文档](https://gitee.com/ios_sub_module/project_functional_components/blob/master/二维码/README.md)

[地图选择展示使用文档](https://gitee.com/ios_sub_module/project_functional_components/blob/master/地图/位置选择展示使用文档.md)


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
