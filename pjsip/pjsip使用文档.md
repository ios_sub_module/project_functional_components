
# pjsip使用文档


## 前沿

* 项目中的视频通话功能用的pisip开源库，具体使用请参考 SPJSua.h 方法中 api 进行相应处理

* pjsip的使用是全局式的，打开app时注册pisip，网络改变重新注册，关闭app即销毁pisip服务。




## 主要内容的介绍

* `语音通话`<br>

* `挂断注消通话`<br>

* `网络切换保持通话`<br>



## pjsip 集成


* 拖拽 “PJSUA2.6” 文件夹到工程中


## 代码介绍 (详细使用，请参考 Demo)

#### 1、在 info.plist 中添加以下字段

* `Privacy - Microphone Usage Description (麦克风权限访问)`<br>



#### 2、pisip使用

* 注册

```Objective-C
  PJSua *pjsua = [PJSua sharedInstance];
        [pjsua registerToServer:[NSString stringWithFormat:@"%@:%@",USERMODEL.sip.sipIp,USERMODEL.sip.sipPort] username:USERMODEL.sip.sipAccount passwd:USERMODEL.sip.sipPwd];
        
   [[PJSua sharedInstance] setVideoCodec];
   
```

* 添加注册通知

```Objective-C
 [HFNoteCenter addObserver:self selector:@selector(onReceiveRegisterResult:) name:PJSUARegisterResultNotification object:nil];
 
 - (void)onReceiveRegisterResult:(NSNotification *)notification {
    
    pjsip_status_code status = [notification.userInfo[@"status"] intValue];
    NSString *statusText = notification.userInfo[@"status_text"];
    
    if (status != PJSIP_SC_OK) {
        
        NSLog(@"登录失败, 错误信息: %d(%@)", status, statusText);
        
        self.isRegisSuccess=NO;
        
        dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC));
        dispatch_after(delayTime, dispatch_get_main_queue(), ^{

            //            [WProgressHudTool wShowAutoDismissWithStatus:NO Title:@"SIP服务注册不成功，请耐心等待"];
            [[PJSua sharedInstance] unregister]; //向服务器发送退出登录
            PJSua *pjsua = [PJSua sharedInstance];
            [pjsua registerToServer:[NSString stringWithFormat:@"%@:%@",USERMODEL.sip.sipIp,USERMODEL.sip.sipPort] username:USERMODEL.sip.sipAccount passwd:USERMODEL.sip.sipPwd];
            [[PJSua sharedInstance] setVideoCodec];


        });

        //        self.isRegisSuccess=NO;
        //     [HFNoteCenter postNotificationName:SipRegisSuccess object:@(NO)];
        
    }else
    {
        
        
        //注册成功修改状态
        //

 
    }
    
}
 
```

* 呼叫


```Objective-C

 [[PJSua sharedInstance] makeAudioCall:self.sendMdl.hotlineno?:self.userphone domain:[NSString stringWithFormat:@"%@:%@",USERMODEL.sip.sipIp,USERMODEL.sip.sipPort]];
 
```


* 挂断


```Objective-C

- (void)hangUp:(NSInteger)phoneID;
 
```

#### 3、网络变化

```Objective-C

 pjsua_call_reinvite((int)[HFUserDefaults integerForKey:CALLID],PJSUA_CALL_REINIT_MEDIA,NULL);
 
```

* * 注销pisp

```Objective-C

 [[PJSua sharedInstance] unregister]; //向服务器发送退出登录
 
```





## 更新介绍




