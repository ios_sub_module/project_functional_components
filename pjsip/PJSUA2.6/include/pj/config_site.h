/*
 * Put this file in pjlib/include/pj
 */

/* sample configure command:
   CFLAGS="-g -Wno-unused-label" ./aconfigure --enable-ext-sound --disable-speex-aec --disable-g711-codec --disable-l16-codec --disable-gsm-codec --disable-g722-codec --disable-g7221-codec --disable-speex-codec --disable-ilbc-codec --disable-opencore-amrnb --disable-sdl --disable-ffmpeg --disable-v4l2
 */
#define PJ_CONFIG_IPHONE 			1
#define PJMEDIA_HAS_VIDEO			1

#include <pj/config_site_sample.h>
