#APP与Web交互协议

> 作者:黄富
> 
> 时间：2018-1-18
> 
> 版本 1.0.9
> 

####更新记录

>1.0.9 加入获取用户信息协议

>1.0.8 加入提示内容不存在对话框协议
>
>1.0.7 加入后台运行管理和自启动管理协议
>
>1.0.6 加入请求提示进度条协议
> 

>1.0.5 加入文件上传协议
>
>1.0.4 加入文件选择协议

>





###web框架

* [DSBridge-IOS](https://github.com/wendux/DSBridge-IOS)

* [DSBridge-Android](https://github.com/wendux/DSBridge-Android)

###URL基本协议

>1.URL链接可带参数

`例如：http://www.baidu.com?statusbarcolor=a & showtoolbar=b`


* 状态栏的颜色(6位rgb或8位argb)

```
key:statusbarcolor;value:ffffffff

```

* 是否显示标题栏

```
key:showtoolbar;value:0

0:显示标题栏；1:隐藏标题栏,默认为0
  
```
### JS交互协议
####使用方式
>基本使用
>
>

``` 
1.引入js脚本
    <script src="https://unpkg.com/flyio/dist/adapter/dsbridge.min.js"></script>
    
2.同步调用方式：dsBridge.call(方法名, 参数)
  
3.异步调用方式：dsBridge.call(方法名, 参数, function (v) {
                 //回调方法
            })  
    
```
>http请求调用

```
1.fly.js请求转发示例需要引入的脚本
    <script src="https://unpkg.com/flyio/dist/fly.min.js"></script>
    <script src="https://unpkg.com/flyio/dist/engine-wrapper.min.js"></script>
    
2.发起请求

 var engine = EngineWrapper(dsbAdapter)
      fly.engine = engine;
      
        function request(){
          fly.get("https://www.baidu.com/").then(function(d){
            alert(d.data)
          })
        }    

```
####基本交互协议
>1.状态栏颜色相关

```
   /**
     * 设置状态栏颜色
     *
     * @param params 颜色值(支持rgb/argb格式) {color:"#ffffffff"}
     */
    -(NSString *)setStatusbarColor:(NSDictionary *) args
    
         
    /**
     * 获取状态栏颜色
     *
     * @param params {}
     * @return 状态栏颜色
     */
    
  -(NSString *)getStatusbarColor:(NSDictionary *) args
```

>2.标题栏相关

```
/**
     * 显示标题栏
     *
     * @param params {}
     */
      -(NSString *) showToolbar:(NSDictionary *) args;
  
    /**
     * 隐藏标题栏
     *
     * @param params {}
     */
      -(NSString *) hideToolbar:(NSDictionary *) args;
   
    /**
     * 标题栏是否显示
     *
     * @param params {}
     * @return true： 显示；false：隐藏
     */
      -(NSString *) isShowToolbar:(NSDictionary *) args;
  
```
>3.窗口控制相关

```
  /**
     * 在新的窗口打开网页
     *
     * @param params {url:"xxxxx"}
     */
     -(void)openInNewWindow:(NSDictionary *) args
  

    /**
     * 关闭窗口
     *
     * @param params 空对象{}
     */
    -(void)closeWindow:(NSDictionary *) args;
    /**
     * 返回上一个页面，没有时则关闭当前页面
     *
     * @param params 空对象{}
     */
    
    -(void)goBack:(NSDictionary *) args
    /**
     * 吐司提示
     *
     * @param params {msg:"xxxx"}
     */
    
    -(void)showToast:(NSDictionary *) args
    
    
```
    

>4.内置http相关


```

 /**
     * 发送get请求(异步)
     *
     * @param params  {url:"xxxx"}
     * @param handler 回调接口
     */
   
-(void)httpGet:(NSDictionary *) args :(void (^)(NSString * _Nullable result,BOOL complete))completionHandler
    
        /**
     * 发送post请求
     *
     * @param params  {
     *                url:"xxxx"
     *                header:{key:value}
     *                forms:{key:value}
     *                }
     * @param handler 回调接口
     * @ 备注
     * 1.url 不能为空
     * 2.header为http请求头，可以为空(不传)
     * 3.params为http表单参数，可以为空(不传)
     */
-(void)httpPost:(NSDictionary *) args :(void (^)(NSString * _Nullable result,BOOL complete))completionHandler

```

>5.其它

```
 /**
     * 获取应用版本号
     *
     * @param params 空对象 {}
     * @return 返回版本号 如 1.0.0
     */
   -(void) getVersion:(NSDictionary *) args
         
     /**
     * 使用第三方浏览器打开
     *
     * @param params {url:"xxxxx"}
     */
  -(void)useThirdBrower:(NSDictionary *) args
    
    

```



>6.加载提示进度条

```
 /**
     * 显示加载进度条
     *
     * @param params{msg:"xxxxxx"} //提示信息
     * @throws JSONException
     */
   
    -(void)showProgressBar:(NSDictionary *) args
    /**
     * 取消加载进度条
     *
     * @param params{}
     */
    
    -(void)dismissProgressBar:(NSDictionary *) args

```

####业务交互协议 

>1.获取token

```

 /**
     * 获取用户token
     *
     * @param params {}
     * @return
     */
   -(NSString *)getToken:(NSDictionary *) args

```


>2.本地文件选择和拍照

```
/**
     * 选择文件
     * <p>
     * 1.默认压缩
     * 2.默认开启调用相机选项
     * 3.默认type为image/*
     * </p>
     *
     * @param params  {isCamera:true //是否调用相机
     *                type:"image/*"//文件过滤类型
     *                width:100   //图片宽度
     *                height:100  //图片高度
     *                }
     * @param handler 回调方法  返回json结果{
     *                path:"xxx"//本地图片路径
     *                imageData:""//base64的图片资源
     *                }
     * @return 返回提示信息
     */
 - ( void )choseFileSelector:(NSDictionary *) args :(void (^)(NSString * _Nullable result,BOOL complete))completionHandler


```

>3.文件上传

```
 /**
     * 文件上传
     *
     * @param params  {
     *                url:"xxxx"//ip地址
     *                data:"path1,path2,..."//文件路径地址拼接
     *                fileKey:"upload" //文件key (必传)
     *                <p>
     *                key:value //其他参数
     *                ...
     *                }
     * @param handler
     */
  - ( void )uploadFiles:(NSDictionary *) args :(void (^)(NSString * _Nullable result,BOOL complete))completionHandler


```

>4.显示内容不存在的对话框

```
 /**
     * 显示内容不存在的对话框，并关闭页面
     *
     * @param params{id:"xx",msg:"xxxx"}
     */
  -(void)showNoFoundDialog:(NSDictionary *) args

```
  
>5.获取用户信息

```
 /**
     * 获取用户信息
     *
     * @param params
     * @return 返回json对象，关键字(account，face，sex，phone，userid，username)
     * 数据为空时，返回{}
     * @throws JSONException
     */
    @JavascriptInterface
    -(NSString *)getUserInfo:(NSDictionary *) args 

```