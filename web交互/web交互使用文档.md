
# web交互使用文档


## 前沿

* 项目中和h5交互用的是dsbridge框架。


## 主要内容的介绍

* `h5调用iOS`<br>

* `iOS调用h5`<br>



## dsbridge 集成


* 拖拽 “dsbridge” 文件夹到工程中


## 代码介绍 (详细使用，请参考 Demo)

#### 实现Api

1、API的实现非常简单，只需要将您要暴漏给js的api放在一个类中，然后统一注册即可。

```Objective-C

//JsApiTest.m
@implementation JsApiTest
- (NSString *) testSyn:(NSDictionary *) args
{
    return [(NSString *)[args valueForKey:@"msg"] stringByAppendingString:@"[ syn call]"];
}

- (void) testAsyn:(NSDictionary *) args :(void (^)(NSString * _Nullable result))handler
{
    handler([(NSString *)[args valueForKey:@"msg"] stringByAppendingString:@"[ asyn call]"]);
}
@end

```

testSyn为同步api, js在调用同步api时会等待native返回，返回后js继续往下执行。

testAsyn为异步api, 异步操作时调用handler.complete通知js，此时js中设置的回调将会被调用。

为了在ios和android平台下兼容，对IOS端Native API接口约定如下：

返回值类型存在时为NSString、不存在时为void。
参数以JSON传递; DSBridge会将js参数自动转化为NSDictionary 
注：JsApiTest.m中实现的方法可以不在JsApiTest.h中声明


#### 注册Api

1、DWebview是sdk中提供的，您也可以使用其它两个，这将在后文介绍。

可见对于Native来说，通过一个单独的类实现js api, 然后直接注册（而不需要像其它一些js bridge每个api都要单独注册），这样不仅非常简单，而且结构清晰。

```Objective-C

DWebview * webview=[[DWebview alloc] initWithFrame:bounds];
jsApi=[[JsApiTest alloc] init];
webview.JavascriptInterfaceObject=jsApi;

```
#### 调用Javascript

1、DWebView提供了两个api用于调用js

```Objective-C
//调用js api(函数)
-(void)callHandler:(NSString *)methodName arguments:(NSArray * _Nullable)args 
                   completionHandler:(void (^)(NSString * _Nullable))completionHandler;
//执行任意js代码
- (void)evaluateJavaScript:(NSString *)javaScriptString 
                  completionHandler:(void (^ _Nullable)(NSString * _Nullable))completionHandler;

```

2.callHandler中，methodName 为js函数名，args为参数数组，可以接受数字、字符串等。

两个函数中completionHandler为完成回调，用于获取js执行的结果。

调用时机

DWebview只有在javascript context初始化成功后才能正确执行js代码，而javascript context初始化完成的时机一般都比整个页面加载完毕要早，随然DSBridge能捕获到javascript context初始化完成的时机，但是一些js api可能声明在页面尾部，甚至单独的js文件中，如果在javascript context刚初始化完成就调用js api, 此时js api 可能还没有加载，所以会失败，为此专门提供了一个api设置一个回调，它会在页面加载结束后调用，为了和didpagefinished区分，我们取名如下：

```Objective-C
- (void)setJavascriptContextInitedListener:(void(^_Nullable)(void))callback;

```
3、若是端上主动调用js，请在此回调中进行 。示例如下：

```Objective-C
__block DWebview * _webview=webview;
[webview setJavascriptContextInitedListener:^(){
  [_webview callHandler:@"test"
  arguments:[[NSArray alloc] initWithObjects:@1,@"hello", nil]
  completionHandler:^(NSString *  value){
      NSLog(@"%@",value);
  }];
}];

```

## 关于DWebview

SDK中有三个webview:

DWKwebview:继承自WKWebView，内部已经实现js prompt、alert、confirm函数对应的对话框。

DUIwebview:继承自UIWebView

DWebview:自定义view, 内部在ios8.0以下会使用DUIwebview, 大于等于8.0会使用DWKwebview。

所有的webview除了都实现了上述api之外，提供了一个加载网页的便捷函数：
```Objective-C
- (void)loadUrl: (NSString *) url;
```

您可以根据具体业务使用任意一个，不过一般情况下优先选用DWebview，它在新设备上更省资源，效率更高。

DWebview还提供了一些其它api和属性，具体请查看其头文件，需要特殊说明的是，有一个api：

```Objective-C
- (id _Nullable) getXWebview;
```

它可以返回DWebview内部使用的真实webview, 值会是DUIwebview和DWKwebview的实例之一，您可以通过isKindOfClass来判断，吃函数主要用于扩展DWebview，下面可以看一下loadRequest的大概实现：


```Objective-C
- (void)loadRequest:(NSURLRequest *)request
{
      id webview=[self getXWebview]；
    if([webview isKindOfClass:[DUIwebview class]]){
        [(DUIwebview *)webview loadRequest:request];
    }else{
        [(DWKwebview *)webview loadRequest:request];
    }
}
```

## 注意

DWebview已经实现 alert、prompt、comfirm对话框，您可以不做处理，也可以自定义。值得一提的是js 在调用alert函数正常情况下只要用户没有关闭alert对话框，js代码是会阻塞的，但是考虑到alert 对话框只有一个确定按钮，也就是说无论用户关闭还是确定都不会影响js代码流程，所以DWebview中在弹出alert对话框时会先给js返回，这样一来js就可以继续执行，而提示框等用户关闭时在关闭即可。如果你就是想要阻塞的alert，可以自定义。而DWebview的prompt、comfirm实现完全符合ecma标准，都是阻塞的。

请不要手动设置DUIwebview的delegate属性，因为DUIwebview在内部已经设置了该属性，如果您需要自己处理页面加载过程，请设置WebEventDelegate属性。

## 更新介绍




